import boto3
import requests
import datetime
import time
import logging
import cloudflare_config
import re

def lambda_handler(event, context):

    # configに登録したドメイン分実行
    count = 0
    while count < len(cloudflare_config.cloudflare_name):
        split_logs = get_cloudflare_logs(cloudflare_config.cloudflare_zoneid[count])
        logs_put(split_logs, cloudflare_config.cloudflare_name[count], cloudflare_config.cloudwatch_log_group_name, cloudflare_config.cloudwatch_log_stream_name[count])
        count += 1

# 各案件
def get_cloudflare_logs(cloudflare_zoneid):

    # 現在時刻
    util_time = datetime.datetime.today()
    util_time = util_time + datetime.timedelta()
    util_time = util_time.strftime("%Y-%m-%dT%H:%M:%SZ")

    # 現在時刻の15分前を作成
    since_time = datetime.datetime.today()
    since_time = since_time + datetime.timedelta(minutes=-15)
    since_time = since_time.strftime("%Y-%m-%dT%H:%M:%SZ")

    # cloudflareログ取得パラメータ設定
    params=(
        # ('action', 'drop'),
        ('source', 'waf'),
        ('since', since_time),
        ('until', util_time),
    )

    # ログ取得先URL
    url = 'https://api.cloudflare.com/client/v4/zones/' + cloudflare_zoneid + '/security/events'

    try:
        # cloudflareログを取得
        response = requests.get(url, headers=cloudflare_config.headers, params=params)
    except:
        print('Error: cloudflareのログ取得に失敗しました')

    try:
        # ログを整形
        result_log = re.sub('      |    |  |"|},|{|],|}|\n\n\n|\n', '', response.text)
        result_log = re.sub('ray_id', ', ray_id', result_log)
        result_log = re.sub('^result.....', '', result_log)
        result_log = re.sub(',', ' ', result_log)
        result_log = re.sub('ray_id', ',ray_id', result_log)
        result_log = re.sub('^,', '', result_log)
    except:
        print("Error: ログの整形に失敗しました")

    # 配列化
    return result_log.split(",")

def logs_put(split_logs, cloudflare_name, cloudwatch_log_group_name, cloudwatch_log_stream_name):
    # 取得したログをcloudwatchへ
    logs_client = boto3.client('logs')
    timestamp = int(round(time.time() * 1000))

    for logs in split_logs:
        token = '';

        try:
            res = logs_client.describe_log_streams(
                      logGroupName=cloudflare_config.cloudwatch_log_group_name,
                      logStreamNamePrefix=cloudflare_config.cloudwatch_log_stream_name,
            )
        except:
            print("Error: cloudwachからのストリーム情報取得に失敗しました")

        try:
            token = res['logStreams'][0]['uploadSequenceToken']
        except KeyError:
            pass

        try:
            if(token == ''):
                result = logs_client.put_log_events(
                    logGroupName=cloudflare_config.cloudwatch_log_group_name,
                    logStreamName=cloudflare_config.cloudwatch_log_stream_name,
                    logEvents=[{
                        'timestamp': timestamp,
                        'message': logs,
                    }]
                )
            elif(token != ''):
                result = logs_client.put_log_events(
                    logGroupName=cloudflare_config.cloudwatch_log_group_name,
                    logStreamName=cloudflare_config.cloudwatch_log_stream_name,
                    logEvents=[{
                        'timestamp': timestamp,
                        'message': logs,
                    }],
                    sequenceToken=token
                )
        except:
            print("Error: cloudwachへのログ挿入に失敗しました")
