# 共通のcloudflare認証
headers = {
    'X-Auth-Email': '',
    'X-Auth-Key': '',
    'Content-Type': 'application/json',
}

# zone idはcloudflare_nameに対応する配列番号にいれること
cloudflare_name = ['']
cloudflare_zoneid = ['']
cloudwatch_log_group_name = ''
cloudwatch_log_stream_name = ''